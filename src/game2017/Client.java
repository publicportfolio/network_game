package game2017;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {
	// HS 10.24.2.116
	// PAW 10.24.65.146
	private static Socket clientSocket;
	private static String serverIP = "10.0.0.7";
	private ClientInputThread input;
	private ClientOutputThread output;

	public void start() throws UnknownHostException, IOException {
		clientSocket = new Socket(serverIP, 6789);
		input = new ClientInputThread(clientSocket);
		output = new ClientOutputThread(clientSocket);
		input.start();
		output.start();
	}

	public ClientOutputThread getOutput() {
		return output;
	}

	public ClientInputThread getInput() {
		return input;
	}

}