package game2017;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayDeque;

public class ClientOutputThread extends Thread {

	private DataOutputStream outToServer;
	private Socket clientSocket;
	private ArrayDeque<String> sentence = new ArrayDeque<String>();

	public ClientOutputThread(Socket clientSocket) {
		this.clientSocket = clientSocket;
	}

	@Override
	public void run() {

		try {

			while (true) {
				outToServer = new DataOutputStream(clientSocket.getOutputStream());
				if (!sentence.isEmpty()) {
					outToServer.writeBytes(sentence.poll() + '\n');
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void updateSentence(String inputSentence) {
		this.sentence.add(inputSentence);
	}
}