package game2017;

import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

public class Main extends Application {

	public static final int size = 20;
	public static final int scene_height = size * 20 + 100;
	public static final int scene_width = size * 20 + 200;

	public static Image image_floor;
	public static Image image_wall;
	public static Image hero_right, hero_left, hero_up, hero_down;
	public static Image fireUp, fireDown, fireLeft, fireRight, hori, verti, fireWallUp, fireWallDown, fireWallLeft,
			fireWallRight, image_melee_up, image_melee_down, image_melee_right, image_melee_left;
	public static Node temp = null;
	private static boolean gunCoolDown;
	private static boolean meleeCoolDown;
	private static boolean wallCoolDown;

	public static Player me;
	public static List<Player> players = new ArrayList<Player>();

	private Label[][] fields;
	private TextArea scoreList;
	private Client client;
	private GridPane boardGrid;

	private String[] board = { // 20x20
			"wwwwwwwwwwwwwwwwwwww", "w        ww        w", "w w  w  www w  w  ww", "w w  w   ww w  w  ww",
			"w  w               w", "w w w w w w w  w  ww", "w w     www w  w  ww", "w w     w w w  w  ww",
			"w   w w  w  w  w   w", "w     w  w  w  w   w", "w ww ww        w  ww", "w  w w    w    w  ww",
			"w        ww w  w  ww", "w         w w  w  ww", "w        w     w  ww", "w  w              ww",
			"w  w www  w w  ww ww", "w w      ww w     ww", "w   w   ww  w      w", "wwwwwwwwwwwwwwwwwwww" };

	// -------------------------------------------
	// | Maze: (0,0) | Score: (1,0) |
	// |-----------------------------------------|
	// | boardGrid (0,1) | scorelist |
	// | | (1,1) |
	// -------------------------------------------

	@Override
	public void start(Stage primaryStage) throws UnknownHostException, IOException {

		client = new Client();
		client.start();

		// timer der tikker i et interval og lytter p� om modifiedSentence er �ndret
		@SuppressWarnings({ "unchecked", "rawtypes" })
		Timeline t = new Timeline(new KeyFrame(Duration.millis(10), new EventHandler() {
			@Override
			public void handle(Event event) {
				checkForReply();
			}
		}));
		t.setCycleCount(Timeline.INDEFINITE);
		t.play();

		try {
			GridPane grid = new GridPane();
			grid.setHgap(10);
			grid.setVgap(10);
			grid.setPadding(new Insets(0, 10, 0, 10));

			Text mazeLabel = new Text("Maze:");
			mazeLabel.setFont(Font.font("Arial", FontWeight.BOLD, 20));

			Text scoreLabel = new Text("Score:");
			scoreLabel.setFont(Font.font("Arial", FontWeight.BOLD, 20));

			scoreList = new TextArea();

			boardGrid = new GridPane();

			image_wall = new Image(getClass().getResourceAsStream("ressource/wall4.png"), size, size, false, false);
			image_floor = new Image(getClass().getResourceAsStream("ressource/floor1.png"), size, size, false, false);

			hero_right = new Image(getClass().getResourceAsStream("ressource/heroRight.png"), size, size, false, false);
			hero_left = new Image(getClass().getResourceAsStream("ressource/heroLeft.png"), size, size, false, false);
			hero_up = new Image(getClass().getResourceAsStream("ressource/heroUp.png"), size, size, false, false);
			hero_down = new Image(getClass().getResourceAsStream("ressource/heroDown.png"), size, size, false, false);

			fireUp = new Image(getClass().getResourceAsStream("ressource/fireUp.png"), size, size, false, false);
			fireDown = new Image(getClass().getResourceAsStream("ressource/fireDown.png"), size, size, false, false);
			fireLeft = new Image(getClass().getResourceAsStream("ressource/fireLeft.png"), size, size, false, false);
			fireRight = new Image(getClass().getResourceAsStream("ressource/fireRight.png"), size, size, false, false);

			hori = new Image(getClass().getResourceAsStream("ressource/fireHorizontal.png"), size, size, false, false);
			verti = new Image(getClass().getResourceAsStream("ressource/fireVertical.png"), size, size, false, false);

			fireWallUp = new Image(getClass().getResourceAsStream("ressource/fireWallNorth.png"), size, size, false,
					false);
			fireWallDown = new Image(getClass().getResourceAsStream("ressource/fireWallSouth.png"), size, size, false,
					false);
			fireWallLeft = new Image(getClass().getResourceAsStream("ressource/fireWallWest.png"), size, size, false,
					false);
			fireWallRight = new Image(getClass().getResourceAsStream("ressource/fireWallEast.png"), size, size, false,
					false);

			image_melee_up = new Image(getClass().getResourceAsStream("ressource/swordUp.png"), size, size, false,
					false);
			image_melee_left = new Image(getClass().getResourceAsStream("ressource/swordLeft.png"), size, size, false,
					false);
			image_melee_right = new Image(getClass().getResourceAsStream("ressource/swordRight.png"), size, size, false,
					false);
			image_melee_down = new Image(getClass().getResourceAsStream("ressource/swordDown.png"), size, size, false,
					false);

			scoreList.setEditable(false);

			grid.add(mazeLabel, 0, 0);
			grid.add(scoreLabel, 1, 0);
			grid.add(boardGrid, 0, 1);
			grid.add(scoreList, 1, 1);

			Scene scene = new Scene(grid, scene_width, scene_height);
			primaryStage.setScene(scene);
			primaryStage.show();

			scene.addEventFilter(KeyEvent.KEY_PRESSED, event -> {
				switch (event.getCode()) {
				case UP:
					// playerMoved(0, -1, "up", me);
					client.getOutput().updateSentence("MOVE|0|-1|up");
					break;
				case DOWN:
					// playerMoved(0, +1, "down", me);
					client.getOutput().updateSentence("MOVE|0|+1|down");
					break;
				case LEFT:
					// playerMoved(-1, 0, "left", me);
					client.getOutput().updateSentence("MOVE|-1|0|left");
					break;
				case RIGHT:
					// playerMoved(+1, 0, "right", me);
					client.getOutput().updateSentence("MOVE|+1|0|right");
					break;
				case Q:
					if (gunCoolDown) {
						System.out.println("COOLDOWN PÅ GUN");
						return;
					} else {
						gunCoolDown = true;
						client.getOutput().updateSentence("SHOOT");
						Timeline timeline = new Timeline(new KeyFrame(Duration.millis(100), ev -> {
							gunCoolDown = false;
						}));
						timeline.setDelay(Duration.seconds(5));
						timeline.setCycleCount(1);
						timeline.play();
					}
					// client.getOutput().updateSentence(me.name);
					break;
				case W:
					if (meleeCoolDown) {
						System.out.println("COOLDOWN PÅ SWORD");
						return;
					} else {
						meleeCoolDown = true;
						client.getOutput().updateSentence("MELEE");
						Timeline timeline = new Timeline(new KeyFrame(Duration.millis(100), ev -> {
							meleeCoolDown = false;
						}));
						timeline.setDelay(Duration.seconds(1));
						timeline.setCycleCount(1);
						timeline.play();
					}
					break;
				case E:
					if (wallCoolDown) {
						System.out.println("COOLDOWN PÅ WALLBREAK");
						return;
					} else {
						wallCoolDown = true;
						client.getOutput().updateSentence("WALL");
						Timeline timeline = new Timeline(new KeyFrame(Duration.millis(100), ev -> {
							wallCoolDown = false;
						}));
						timeline.setDelay(Duration.seconds(1));
						timeline.setCycleCount(1);
						timeline.play();
					}
				default:
					break;
				}
			});
			scoreList.setText(getScoreList());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public void fireDatGunBuddy() {
		int x = me.getXpos(), y = me.getYpos();
		int i = 0;
		ArrayList<Label> modifiedFields = new ArrayList<Label>();
		while (true) {
			if (me.direction.equals("up")) {
				Player p = getPlayerAt(x, y - 1);
				if (y == 0) {
					y = 20;
				}
				if (board[y - 1].charAt(x) != 'w' && p == null) {
					if (i == 0) {
						fields[x][y - 1].setGraphic(new ImageView(fireUp));
					} else {
						fields[x][y - 1].setGraphic(new ImageView(verti));
					}
					modifiedFields.add(fields[x][y - 1]);
					y--;
					if (y == 0) {
						y = 20;
					}
				} else {
					if (y != 20) {
						if (p != null) {
							p.addPoints(-50);
							me.addPoints(50);
							if (i != 0) {
								fields[x][y].setGraphic(new ImageView(fireWallUp));
							}
						} else {
							me.addPoints(-25);
							if (i != 0) {
								fields[x][y].setGraphic(new ImageView(fireWallUp));
							}
						}
					}
					break;
				}
				i++;
			} else if (me.direction.equals("down")) {
				Player p = getPlayerAt(x, y + 1);
				if (y == 19) {
					y = -1;
				}
				if (board[y + 1].charAt(x) != 'w' && p == null) {
					if (i == 0) {
						fields[x][y + 1].setGraphic(new ImageView(fireDown));
					} else {
						fields[x][y + 1].setGraphic(new ImageView(verti));
					}
					modifiedFields.add(fields[x][y + 1]);
					y++;
					if (y == 19) {
						y = -1;
					}
				} else {
					if (y != -1) {
						if (p != null) {
							p.addPoints(-50);
							me.addPoints(50);
							if (i != 0) {
								fields[x][y].setGraphic(new ImageView(fireWallDown));
							}
						} else {
							me.addPoints(-25);
							if (i != 0) {
								fields[x][y].setGraphic(new ImageView(fireWallDown));
							}
						}
					}
					break;
				}
				i++;
			} else if (me.direction.equals("right")) {
				Player p = getPlayerAt(x + 1, y);
				if (x == 19) {
					x = -1;
				}
				if (board[y].charAt(x + 1) != 'w' && p == null) {
					if (i == 0) {
						fields[x + 1][y].setGraphic(new ImageView(fireRight));
					} else {
						fields[x + 1][y].setGraphic(new ImageView(hori));
					}
					modifiedFields.add(fields[x + 1][y]);
					x++;
					if (x == 19) {
						x = -1;
					}
				} else {
					if (x != -1) {
						if (p != null) {
							p.addPoints(-50);
							me.addPoints(50);
							if (i != 0) {
								fields[x][y].setGraphic(new ImageView(fireWallRight));
							}
						} else {
							me.addPoints(-25);
							if (i != 0) {
								fields[x][y].setGraphic(new ImageView(fireWallRight));
							}
						}
					}
					break;
				}
				i++;
			} else {
				Player p = getPlayerAt(x - 1, y);
				if (x == 0) {
					x = 20;
				}
				if (board[y].charAt(x - 1) != 'w' && p == null) {
					if (i == 0) {
						fields[x - 1][y].setGraphic(new ImageView(fireLeft));
					} else {
						fields[x - 1][y].setGraphic(new ImageView(hori));
					}
					modifiedFields.add(fields[x - 1][y]);
					x--;
					if (x == 0) {
						x = 20;
					}
				} else {
					if (x != 20) {
						if (p != null) {
							p.addPoints(-50);
							me.addPoints(50);
							if (i != 0) {
								fields[x][y].setGraphic(new ImageView(fireWallLeft));
							}
						} else {
							me.addPoints(-25);
							if (i != 0) {
								fields[x][y].setGraphic(new ImageView(fireWallLeft));
							}
						}
					}
					break;
				}
				i++;
			}
		}
		@SuppressWarnings("rawtypes")
		Timeline t = new Timeline(new KeyFrame(Duration.millis(2), new EventHandler() {

			@Override
			public void handle(Event event) {
				for (Label l : modifiedFields) {
					ImageView temp = (ImageView) l.getGraphic();
					if (temp.getImage().equals(hero_up) || temp.getImage().equals(hero_down)
							|| temp.getImage().equals(hero_left) || temp.getImage().equals(hero_right)) {

					} else {
						l.setGraphic(new ImageView(image_floor));
					}
				}
			}
		}));
		t.setDelay(Duration.seconds(0.5));
		t.setCycleCount(1);
		t.play();
		scoreList.setText(getScoreList());
	}

	public void playerMoved(int delta_x, int delta_y, String direction) {
		me.direction = direction;
		int x = me.getXpos(), y = me.getYpos();
		if (y + delta_y == 21 || x + delta_x == 21 || y + delta_y == -1 || x + delta_x == -1) {
			return;
		}
		if (board[y + delta_y].charAt(x + delta_x) == 'w') {
			me.addPoints(-1);
			if (direction.equals("right")) {
				fields[x][y].setGraphic(new ImageView(hero_right));
			}
			;
			if (direction.equals("left")) {
				fields[x][y].setGraphic(new ImageView(hero_left));
			}
			;
			if (direction.equals("up")) {
				fields[x][y].setGraphic(new ImageView(hero_up));
			}
			;
			if (direction.equals("down")) {
				fields[x][y].setGraphic(new ImageView(hero_down));
			}
		} else {
			Player p = getPlayerAt(x + delta_x, y + delta_y);
			if (p != null) {
				me.addPoints(10);
				p.addPoints(-10);
				if (direction.equals("right")) {
					fields[x][y].setGraphic(new ImageView(hero_right));
				}
				;
				if (direction.equals("left")) {
					fields[x][y].setGraphic(new ImageView(hero_left));
				}
				;
				if (direction.equals("up")) {
					fields[x][y].setGraphic(new ImageView(hero_up));
				}
				;
				if (direction.equals("down")) {
					fields[x][y].setGraphic(new ImageView(hero_down));
				}
			} else {
				me.addPoints(1);

				fields[x][y].setGraphic(new ImageView(image_floor));
				x += delta_x;
				y += delta_y;

				if (direction.equals("right")) {
					fields[x][y].setGraphic(new ImageView(hero_right));
				}
				;
				if (direction.equals("left")) {
					fields[x][y].setGraphic(new ImageView(hero_left));
				}
				;
				if (direction.equals("up")) {
					fields[x][y].setGraphic(new ImageView(hero_up));
				}
				;
				if (direction.equals("down")) {
					fields[x][y].setGraphic(new ImageView(hero_down));
				}
				;

				me.setXpos(x);
				me.setYpos(y);
			}
		}

		scoreList.setText(getScoreList());
	}

	public String getScoreList() {
		StringBuffer b = new StringBuffer(100);
		for (Player p : players) {
			b.append(p + "\r\n");
		}
		return b.toString();
	}

	public Player getPlayerAt(int x, int y) {
		for (Player p : players) {
			if (p.getXpos() == x && p.getYpos() == y) {
				return p;
			}
		}
		return null;
	}

	public static void main(String[] args) {
		launch(args);
	}

	public Player getPlayerByName(String name) {
		Player player = null;
		for (int i = 0; i < players.size(); i++) {
			if (players.get(i) != null && players.get(i).name.equalsIgnoreCase(name)) {
				player = players.get(i);
			}
		}
		return player;
	}

	public void checkForReply() {
		if (!client.getInput().getModifiedSentence().isEmpty()) {
			String[] splited = client.getInput().getModifiedSentence().poll().split("[|]");
			if (splited[0].equals("MOVE")) {
				String x = splited[1].replace("+", "");
				String y = splited[2].replace("+", "");
				String direction = splited[3];
				playerMoved(Integer.parseInt(x), Integer.parseInt(y), direction);
			} else if (splited[0].equals("SHOOT")) {
				fireDatGunBuddy();
			} else if (splited[0].equals("MELEE")) {
				useMeleeWeapon();
			} else if (splited[0].equals("WALL")) {
				breakWall();
			} else if (splited[0].equals("MAP")) {
				setBoard(splited);
			}
		}
	}

	public void useMeleeWeapon() {

		int[] position = new int[2];

		if (me.direction.equals("up")) {
			temp = fields[me.getXpos()][me.getYpos() - 1].getGraphic();
			fields[me.getXpos()][me.getYpos() - 1].setGraphic(new ImageView(image_melee_up));
			position[0] = me.getXpos();
			position[1] = me.getYpos() - 1;
		} else if (me.direction.equals("down")) {
			temp = fields[me.getXpos()][me.getYpos() + 1].getGraphic();
			fields[me.getXpos()][me.getYpos() + 1].setGraphic(new ImageView(image_melee_down));
			position[0] = me.getXpos();
			position[1] = me.getYpos() + 1;
		} else if (me.direction.equals("left")) {
			temp = fields[me.getXpos() - 1][me.getYpos()].getGraphic();
			fields[me.getXpos() - 1][me.getYpos()].setGraphic(new ImageView(image_melee_left));
			position[0] = me.getXpos() - 1;
			position[1] = me.getYpos();
		} else {
			temp = fields[me.getXpos() + 1][me.getYpos()].getGraphic();
			fields[me.getXpos() + 1][me.getYpos()].setGraphic(new ImageView(image_melee_right));
			position[0] = me.getXpos() + 1;
			position[1] = me.getYpos();
		}

		Timeline timeline = new Timeline(new KeyFrame(Duration.millis(100), ev -> {
			fields[position[0]][position[1]].setGraphic(temp);
		}));
		timeline.setCycleCount(1);
		timeline.play();

	}

	public void setBoard(String[] input) {
		for (int i = 0; i < 20; i++) {
			board[i] = input[i + 1];
		}
		fields = new Label[board.length][board.length];
		for (int j = 0; j < board.length; j++) {
			for (int i = 0; i < board.length; i++) {
				switch (board[j].charAt(i)) {
				case 'w':
					fields[i][j] = new Label("", new ImageView(image_wall));
					break;
				case ' ':
					fields[i][j] = new Label("", new ImageView(image_floor));
					break;
				default:
					try {
						throw new Exception("Illegal field value: " + board[j].charAt(i));
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				boardGrid.add(fields[i][j], i, j);
			}
		}
		setPlayers();
		String music = "src/game2017/sound/Sea Shanty 2.mp3";
		Media sound = new Media(new File(music).toURI().toString());
		MediaPlayer mediaPlayer = new MediaPlayer(sound);
		mediaPlayer.setCycleCount(Timeline.INDEFINITE);
		mediaPlayer.play();
	}

	public void setPlayers() {
		// Setting up standard players

		Player henrik = new Player("Henrik", 14, 15, "left", "10.24.2.116");
		players.add(henrik);
		fields[14][15].setGraphic(new ImageView(hero_left));

		me = new Player("Paw", 1, 1, "left", "10.24.65.146");
		players.add(me);
		fields[1][1].setGraphic(new ImageView(hero_left));

		// Player aleta = new Player("Aleta", 3, 2, "up", "10.24.66.173");
		// players.add(aleta);
		// fields[3][2].setGraphic(new ImageView(hero_up));
	}

	public void breakWall() {
		int x = me.getXpos();
		int y = me.getYpos();
		int[] temp = new int[2];
		temp[0] = 400;
		if (me.direction.equals("up") && board[y - 1].charAt(x) == 'w') {
			temp[0] = y - 1;
			temp[1] = x;
			board[y - 1] = board[y - 1].substring(0, x) + " " + board[y - 1].substring(x + 1, 20);
			fields[x][y - 1].setGraphic(new ImageView(image_floor));
		} else if (me.direction.equals("down") && board[y + 1].charAt(x) == 'w') {
			temp[0] = y + 1;
			temp[1] = x;
			board[y + 1] = board[y + 1].substring(0, x) + " " + board[y + 1].substring(x + 1, 20);
			fields[x][y + 1].setGraphic(new ImageView(image_floor));
		} else if (me.direction.equals("right") && board[y].charAt(x + 1) == 'w') {
			temp[0] = y;
			temp[1] = x + 1;
			board[y] = board[y].substring(0, x + 1) + " " + board[y].substring(x + 2, 20);
			fields[x + 1][y].setGraphic(new ImageView(image_floor));
		} else if (me.direction.equals("left") && board[y].charAt(x - 1) == 'w') {
			temp[0] = y;
			temp[1] = x - 1;
			board[y] = board[y].substring(0, x - 1) + " " + board[y].substring(x, 20);
			fields[x - 1][y].setGraphic(new ImageView(image_floor));
		}

		@SuppressWarnings({ "rawtypes", "unchecked" })
		Timeline t = new Timeline(new KeyFrame(Duration.millis(10), new EventHandler() {

			@Override
			public void handle(Event event) {
				board[temp[0]] = board[temp[0]].substring(0, temp[1]) + "w" + board[temp[0]].substring(temp[1] + 1, 20);
				fields[temp[1]][temp[0]].setGraphic(new ImageView(image_wall));
				// fix det med at gå ud af mappet og hvis du står et sted hvor der skal være en
				// væg
			}
		}));
		t.setDelay(Duration.seconds(3));
		t.setCycleCount(1);
		if (temp[0] != 400) {
			t.play();
		}

	}
}