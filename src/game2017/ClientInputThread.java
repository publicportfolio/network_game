package game2017;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.ArrayDeque;

public class ClientInputThread extends Thread {
	private BufferedReader inFromServer;
	private ArrayDeque<String> modifiedSentence = new ArrayDeque<String>();
	private Socket clientSocket;

	public ClientInputThread(Socket clientSocket) {
		this.clientSocket = clientSocket;
	}

	@Override
	public void run() {
		try {
			inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			while (true) {
				modifiedSentence.add(inFromServer.readLine());
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public ArrayDeque<String> getModifiedSentence() {
		return modifiedSentence;
	}

}