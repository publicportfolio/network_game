package game2017;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Random;

public class Server {
	private static ArrayList<Socket> sockets = new ArrayList<>();
	private static ArrayList<Player> players = new ArrayList<>();
	private static ArrayList<DataOutputStream> outputStreams = new ArrayList<DataOutputStream>();
	private static String map;

	public static void main(String[] args) throws Exception {
		TempClass t = new TempClass();
		ServerSocket welcomSocket = new ServerSocket(6789);
		map = createMap(20);

		Thread serverThread = new Thread() {

			@Override
			public void run() {

				while (true) {
					try {
						Socket connectionSocket = welcomSocket.accept();

						sockets.add(connectionSocket);
						outputStreams.add(new DataOutputStream(connectionSocket.getOutputStream()));
						outputStreams.get(outputStreams.size() - 1).writeBytes(map + "\n");

						Thread server1 = new Thread() {
							private String hej = this.toString();

							@Override
							public void run() {
								try {
									String clientSentence = "";
									String resultToClient = "";
									BufferedReader inFromClient = new BufferedReader(
											new InputStreamReader(connectionSocket.getInputStream()));

									while (true) {
										t.letGo(clientSentence, resultToClient, inFromClient, outputStreams);
									}
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

							}
						};
						server1.start();

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			}
		};
		serverThread.start();

	}

	public static String createMap(int size) {
		String result = "MAP";
		Random ran = new Random();
		for (int i = 0; i < size; i++) {
			result += "|";
			for (int j = 0; j < size; j++) {
				int help = ran.nextInt(3);
				if (help == 1) {
					result += "w";
				} else {
					result += " ";
				}
			}
		}
		return result;
	}
}