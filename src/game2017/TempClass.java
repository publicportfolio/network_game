package game2017;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class TempClass {

	public synchronized void letGo(String clientSentence, String resultToClient, BufferedReader inFromClient,
			ArrayList<DataOutputStream> outputStreams) throws IOException {
		if (inFromClient.ready()) {
			clientSentence = inFromClient.readLine().trim();
			resultToClient = clientSentence;

			if (!resultToClient.isEmpty()) {
				for (DataOutputStream d : outputStreams) {
					System.out.println(resultToClient);
					d.writeBytes(resultToClient + "\n");
				}
				resultToClient = "";
			}
		}
	}
}
